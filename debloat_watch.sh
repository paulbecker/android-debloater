#!/bin/sh
adb shell pm uninstall -k --user 0 com.samsung.android.bixby.agent
adb shell pm uninstall -k --user 0 com.samsung.android.bixby.wakeup
adb shell pm uninstall -k --user 0 google.android.wearable.assistant
adb shell pm uninstall -k --user 0 com.google.android.wearable.assistant
adb shell pm uninstall -k --user 0 com.samsung.android.gallery.watch
